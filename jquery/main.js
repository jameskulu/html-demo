

$(document).ready(function() {


    const heading = $('.heading').text()
    // console.log(heading)
    // alert(heading)

    const a = $('h1:first').text()
    // alert(a)

    const paragraph = $('p.intro').text()
    const div = $('.main .heading').text()
    // alert(div)
})



// Events

$('.submit-btn').hover(function(){
    console.log("Clicked")
})



// hide / show

$('.submit-btn').click(function(){
    $('#head1').text('new changed heading')
    // $('.heading').hide(1000)
    // $('.heading').show(1000)
})


// Get / Set text


// getting text
let heading1 = $('#head1').text() //html
// console.log(heading1)


//setting text
// $('#head1').text('new changed heading')


//getting value
const input = $('#input-field').val()
console.log(input)


// Add/Remove element
	// - append() - after content
	// - prepend() - before content
	// - after() - after element
	// - before() - before element
	// - remove()
	// - empty()

// $('#head1').append(`
//     <p>Sentence</p>
//     <p>Sentence</p>
//     <p>Sentence</p>
//     <p>Sentence</p>
//     <p>Sentence</p>
//     <p>Sentence</p>
//     <p>Sentence</p>
//     <p>Sentence</p>
// `)
// $('#head1').prepend('first sentence')
// $('#head1').after('after')
// $('#head1').before('before')


// $('#head1').remove()
// $('#head1').empty()


// - CSS Class
// 	- $('#div').addClass('hello')
// 	- $('#div').removeClass('hello')
// 	- $('#div').toggleClass('hello')
// 	- $('#div').css('background-color', 'red')

// $('#head1').toggleClass('hello')


// $('#head1').css({
//     'font-size' : '10px',
//     'width' : '30px',
//     'height' : '10px'
//  });


// - Traversing
// 	- $('#div').parent()
// 	- $('#div').parents()
// 	- $('#div').parentsUntil('hello')
// 	- $('#div').children()
// 	- $('#div').find()
// 	- siblings
// 	- next()
// 	- nextAll()
// 	- nextUntil('div')
//  - prev()
//  - prevAll()
    
const head = $('#head1').next()
console.log(head)



