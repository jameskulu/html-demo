let full_name = 'James' 
let number = '9213123213' 
let age = 40

age = 30


const company = 'InfoDevelopers'

console.log(company)





// DOM - Document object model

console.log(document.body)



// DOM Selectors

const btn = document.getElementById('btn')
// const paragraph = document.getElementsByClassName('paragraph')
// const paragraph = document.getElementsByTagName('a')
// const paragraph = document.querySelector('.btn')





// DOM Elements

// - addEventListener
// 	- innerText
// 	-innerHTML
// 	-textContent
// 	- classlist.add, remove
// 	-style
// 	-getAttribute, setAttribute, removeAttribute
// 	-append
// 	-appendChild
// 	-remove



const div = document.createElement('div')

div.innerHTML = '<button id="btn2">Click Me</button>'


// div.classList.remove('new-class')
div.classList.add('new-class')

const a = div.getAttribute('class')
div.setAttribute('class', 'new_one new_two')
div.setAttribute('id', 'new_id')
div.removeAttribute('id', 'new_id')


// document.body.append('hello')
document.body.appendChild(div)


// div.remove()

div.style.height = '100px'
div.style.width = '100px'
div.style.backgroundColor = 'red'


// console.log(paragraph)



// DOM Events
	// -click
	// -mouseup
	// -mousedown
	// -mouseover
	// -keyup
	// -keydown
	// -submit
	// -change



btn.addEventListener('keyup', function(){
    div.style.height = '500px'

})


const checkbox = document.querySelector('#checkbox')

checkbox.addEventListener('change', function(){
    console.log("checked")
})

